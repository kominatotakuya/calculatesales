package jp.alhinc.kominato_takuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class CalculateSales {
	public static void main(String[] args) {
		if(args.length != 1) {                                   
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> StoreMap = new HashMap<String, String>();
		Map<String, Long> SalesMap = new HashMap<String, Long>();  //集計用map
		
		//指定したファイルの入力メソッド
		if(!fileInput(args[0],"branch.lst","支店","[0-9]{3}",StoreMap,SalesMap)) {
			return;
		}
		
		ArrayList<File> salesFileList = new ArrayList<File>();
		
		String path = args[0];  
		File dir = new File(path);  
		File[] files = dir.listFiles();
		for(int i = 0; i < files.length; i++) {		
			String str = files[i].getName();                      
			if(files[i].isFile() && str.matches("^[0-9]{8}.rcd$")) {                    
				salesFileList.add(files[i]);
			}
		}
		 //売上ファイルの数字部分のみ抜き出して連番のチェック
		for(int i = 0; i < salesFileList.size() - 1; i++) {
			String fileName = salesFileList.get(i).getName();
			int exIndex = fileName.lastIndexOf(".");
			int n = Integer.parseInt(fileName.substring(0, exIndex));
		
			String nextFileName = salesFileList.get(i+1).getName();
			int exIndex2 = nextFileName.lastIndexOf(".");
			int t = Integer.parseInt(nextFileName.substring(0, exIndex2));
			
			if(t - n != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//集計用メソッド
		if(!sumSales(salesFileList,SalesMap)) {
			return;
		}
		
		//集計用mapをファイルに出力するメソッド
		if(!fileOutput(args[0],"branch.out", StoreMap,SalesMap)) {
			return;
		}
	
	
	}
	 public static boolean fileOutput(String path, String fileName, Map<String,String> numberNameMap, Map<String,Long> totalMap) {
		PrintWriter pw = null;
		try{
				File file = new File(path,fileName);
				FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
				pw = new PrintWriter(bw);
				for(Entry<String,String> entry :numberNameMap.entrySet()) {
						pw.println(entry.getKey() +  "," +  entry.getValue() + "," + totalMap.get(entry.getKey()));
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally {
				if(pw != null) {
						pw.close();
					}
			}	
		return true;
	}
	 
	 public static boolean fileInput(String path,String fileName,String word,String chord,Map<String,String> numberNameMap,Map<String,Long> totalMap){
		BufferedReader br = null;   
		try {                        
			File file = new File(path, fileName); 
			if(!file.exists()) {
				System.out.println(word + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr =new FileReader(file);   
			br = new BufferedReader(fr); 
			String line; 
			while((line = br.readLine()) != null) {
				String[] store = line.split(",");                  
				if(store[0].matches(chord)) {                  
					if(store.length != 2){
						System.out.println(word + "定義ファイルのフォーマットが不正です");
						return false;
					}
					numberNameMap.put(store[0], store[1]);   
					totalMap.put(store[0], 0L);         
				}else {
					System.out.println(word + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch(IOException e) {  
			System.out.println("予期せぬエラーが発生しました");
			return false;
			
		}finally {  
			if(br != null) { 
				try {
					br.close();
				}catch(IOException e) { 
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	 }

	 public static boolean sumSales(ArrayList<File> salesFileList,Map<String,Long> totalMap){
		 
		 for(int i = 0; i < salesFileList.size(); i++) {
				ArrayList<String> salesList2 = new ArrayList<String>();  
				BufferedReader br = null;
				try {                        
					FileReader fr =new FileReader(salesFileList.get(i));   
					br = new BufferedReader(fr); 
					String line; 
					while((line = br.readLine()) != null) { 
						salesList2.add(line);               
					    if(totalMap.containsKey(salesList2.get(0))) {  
					    }else {
					    	System.out.println(salesFileList.get(i).getName() + "の支店コードが不正です");
							return false;
					    }
					}
					if(salesList2.size() != 2) {
						System.out.println(salesFileList.get(i).getName() + "のフォーマットが不正です");
						return false;
					}
					if(salesList2.get(1).matches("^[0-9]*$")) {
						Long sum = totalMap.get(salesList2.get(0)) + Long.parseLong(salesList2.get(1));
						if(String.valueOf(sum).length() > 10) {
							System.out.println("合計金額が10桁を超えました");
							return false;
						}
						totalMap.put(salesList2.get(0), sum);
					}else {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}catch(IOException e) { 
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}finally {  
					if(br != null) { 
						try {
							br.close();
						}catch(IOException e) { 
							System.out.println("予期せぬエラーが発生しました");
						}
					}
				}       			
			}
		 return true;
		 }
}
